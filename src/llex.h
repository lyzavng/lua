/*
** $Id: llex.h $
** Lexical Analyzer
** See Copyright Notice in lua.h
*/

#ifndef llex_h
#define llex_h

#include "lobject.h"
#include "lzio.h"


#define FIRST_RESERVED	257


#if !defined(LUA_ENV)
#define LUA_ENV		"_ENV"
#endif


/*
* WARNING: if you change the order of this enumeration,
* 警告:如果更改此枚举的顺序，grep "保留字列表"
* grep "ORDER RESERVED"
*/
enum RESERVED {
/* terminal symbols denoted by reserved words用保留字表示的终端符号
*  这个是枚举, FIRST_RESERVED在上面定义成了257,那么按顺序TK_WHILE为278
*  他们相减后加1为这半段保留字的个数22.
*/
  TK_AND = FIRST_RESERVED, TK_BREAK,
  TK_DO, TK_ELSE, TK_ELSEIF, TK_END, TK_FALSE, TK_FOR, TK_FUNCTION,
  TK_GOTO, TK_IF, TK_IN, TK_LOCAL, TK_NIL, TK_NOT, TK_OR, TK_REPEAT,
  TK_RETURN, TK_THEN, TK_TRUE, TK_UNTIL, TK_WHILE,
  /* other terminal symbols 其他终端符号*/
  TK_IDIV, TK_CONCAT, TK_DOTS, TK_EQ, TK_GE, TK_LE, TK_NE,
  TK_SHL, TK_SHR,
  TK_DBCOLON, TK_EOS,
  TK_FLT, TK_INT, TK_NAME, TK_STRING
};

/* number of reserved words 保留字数 */
#define NUM_RESERVED	(cast_int(TK_WHILE-FIRST_RESERVED + 1))


typedef union {
  lua_Number r;
  lua_Integer i;
  TString *ts;
} SemInfo;  /* semantics information 语义信息 */


typedef struct Token {
  int token;
  SemInfo seminfo;
} Token;    /* 符记 */


/* state of the lexer plus state of the parser when shared by all
*  functions当所有函数共享时，lexer的状态加上解析器的状态
*/
typedef struct LexState {
  int current;  /* current character (charint) 当前读入的字符 */
  int linenumber;  /* input line counter输入的行的计数器 */
  int lastline;  /* line of last token 'consumed' 上一个行 */
  Token t;  /* current token当前对比token */
  Token lookahead;  /* look ahead token */
  struct FuncState *fs;  /* current function (parser) FuncState 解析器私有值 */
  struct lua_State *L;   /* lua_State虚拟机全局状态机 */
  ZIO *z;  /* input stream 这里可以通过lzio.h看出，这里是字符流，通过这里读取输入的字符流 */
  Mbuffer *buff;  /* buffer for tokens tokens的流存储器 */
  Table *h;  /* to avoid collection/reuse strings避免收集/重用字符串 */
  struct Dyndata *dyd;  /* dynamic structures used by the parser解析器使用的动态结构 */
  TString *source;  /* current source name当前源名称 */
  TString *envn;  /* environment variable name环境变量名 */
} LexState;


LUAI_FUNC void luaX_init (lua_State *L);
LUAI_FUNC void luaX_setinput (lua_State *L, LexState *ls, ZIO *z,
                              TString *source, int firstchar);
LUAI_FUNC TString *luaX_newstring (LexState *ls, const char *str, size_t l);
LUAI_FUNC void luaX_next (LexState *ls);
LUAI_FUNC int luaX_lookahead (LexState *ls);
LUAI_FUNC l_noret luaX_syntaxerror (LexState *ls, const char *s);
LUAI_FUNC const char *luaX_token2str (LexState *ls, int token); /* 符记转字符 */


#endif
