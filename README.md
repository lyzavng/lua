# lua

#### 介绍
Lua 5.4 中文版,可以中文编程,并用中文关键字及中文函数等.

Lua是一个功能强大的，高效的，轻量级的，可嵌入的脚本由开发语言 队 在 市局-里约热内卢，在巴西里约热内卢天主教大学。Lua是 全世界许多产品和项目中 使用的 免费软件。
Lua的 官方网站 提供了有关Lua的完整信息，包括 执行摘要 和更新的 文档（尤其是 参考手册），这些信息可能与 此软件包中分发的本地副本略有不同 。

Lua 5.4于2020年6月29日发布。它的 主要新功能 是用于垃圾收集以及const和将要关闭的变量的新一代模式。
当前版本为 Lua 5.4.0，于2020年6月29日发布。

#### 自Lua 5.3起的更改
以下是Lua 5.4中引入的主要更改。该 参考手册 列出了 不兼容，必须予以介绍。

主要变化
新一代垃圾收集模式
待封闭变量
const变量
userdata可以具有多个用户值
math.random的新实现
预警系统
调试有关函数参数和返回的信息
整数“ for”循环的新语义
'string.gmatch'的可选'init'参数
新功能'lua_resetthread'和'coroutine.close'
字符串到数字的强制转换到字符串库
缩小内存块时分配功能失败
'string.format'中的新格式'％p'
utf8库最多接受2 ^ 31个代码点

#### 简单安装

下载源码，如本网，　点击　克隆／下载，　点击下载zip，
解压（最好解压在你的home文件夹下的一个目录），点右键解压，选择位置即可．
用终端打开相应的文件夹，　在相应文件夹下点右键，点击＂在终端中打开＂．
执行命令　sudo make all test
        sudo make install

#### 安装教程
Lua以源代码 形式分发 。您需要先构建它，然后再使用它。构建Lua应该很简单，因为Lua是在纯ANSI C中实现的，并且在具有ANSI C编译器的所有已知平台中都可以未经修改地进行编译。Lua还将未经修改地编译为C ++。下面给出的构建Lua的说明适用于类似Unix的平台，例如Linux和Mac OSX。另请参见有关 其他系统 和 自定义选项的说明。

如果您没有时间或意愿自己编译Lua，请从LuaBinaries获取二进制 文件。还可以尝试 LuaDist，它是Lua的多平台发行版，其中包括电池。

建筑楼
在大多数常见的类似Unix的平台上，只需执行“ make”。这是详细信息。

打开一个终端窗口，然后移到顶层目录，名为lua-5.4.0。此处的Makefile同时控制构建过程和安装过程。
做“ make”。该Makefile文件会猜测你的平台和建立的Lua它。
如果猜测失败，请执行“ make help”，然后查看您的平台是否已列出。当前支持的平台是：
猜测aix bsd c89 freebsd通用linux linux-readline macosx mingw posix solaris

如果您的平台已列出，则只需执行“ make xxx”，其中xxx是您的平台名称。

如果未列出您的平台，请按此顺序尝试最接近的一个或posix（通用c89）。

编译仅需几分钟，即可在src目录中生成三个文件：lua（解释器），luac（编译器）和liblua.a（库）。
要检查Lua是否已正确构建，请make test在构建Lua之后执行“ ”。这将运行解释器并打印其版本。
如果您正在运行Linux，请尝试使用“ make linux-readline”来构建具有方便的行编辑和历史记录功能的交互式Lua解释器。如果遇到编译错误，请确保已安装readline开发包（可能名为libreadline-dev或readline-devel）。如果之后出现链接错误，请尝试“ make linux-readline MYLIBS=-ltermcap”。

#### 安装lua

一旦构建了Lua，您可能希望将其安装在系统的正式位置。在这种情况下，请执行“ make install”。正式位置和安装文件的方式在Makefile中定义。您可能需要正确的权限才能安装文件，因此可能需要执行“ sudo make install”。

要一步一步构建和安装Lua，请执行“ make all install”或“ make xxx install”，其中xxx是您的平台名称。

要在构建Lua之后在本地安装Lua，请执行“ make local”。这将创建一个包含子目录 bin，include，lib，man，share的目录安装，并安装Lua，如下所示。要在本地安装Lua，但在其他目录中，请执行“ ”，其中xxx是您选择的目录。安装从src和doc目录开始，因此请注意INSTALL_TOP不是绝对路径。 make install INSTALL_TOP=xxx

bin:
lua luac
include:
lua.h luaconf.h lualib.h lauxlib.h lua.hpp
lib:
liblua.a
man/man1:
lua.1 luac.1
这些是开发所需的唯一目录。如果只想运行Lua程序，则只需要bin和man中的文件。要将Lua嵌入C或C ++程序中，需要include和lib中的文件。

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 执照
Lua是根据 以下MIT许可条款分发的免费软件 ；它可以毫无问题地用于任何目的，包括商业目的，而无需询问我们。唯一的要求是，如果您确实使用Lua，则应通过在产品或其文档中的某些位置包含适当的版权声明来给予我们信誉。有关详细信息，请参阅此。

版权所有©1994–2020 Lua.org，PUC-Rio。
特此免费授予获得此软件和相关文档文件（“软件”）副本的任何人无限制地处理软件的权利，包括但不限于使用，复制，修改，合并的权利，发布，分发，再许可和/或出售本软件的副本，并允许配备有本软件的人员这样做，但须满足以下条件：

以上版权声明和本许可声明应包含在本软件的所有副本或大部分内容中。

本软件按“原样”提供，不提供任何形式的明示或暗示担保，包括但不限于对适销性，特定目的的适用性和非侵权性的保证。无论是由于软件，使用或其他方式产生的，与之有关或与之有关的合同，侵权或其他形式的任何索赔，损害或其他责任，作者或版权所有者概不负责。软件。

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
